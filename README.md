Exemple d’utilisation de [HomeAssistant](https://www.home-assistant.io/) et des intégrations dans Docker/Podman.

# Dépendances
Cette méthode utilise [Docker](https://www.docker.com/) ou [Podman](https://podman.io/) et [docker-compose](https://docs.docker.com/compose/) ou [podman-compose](https://github.com/containers/podman-compose).

# Containers
## Home Assistant
C’est le cœur du serveur.

Le serveur doit utiliser le réseau _host_ car il utilise de nombreux ports réseau, notamment pour détecter les éléments à intégrer.
Des intégrations peuvent aussi ouvrir des connexions.

## DataBase
La base de données offre une meilleure mémoire que SQLite.
Cependant la base de données MariaDB est parfois long à initialisé.
La configuration de Home Assistant (`configuration.yaml`) doit être modifiée selon le [wiki](https://www.home-assistant.io/integrations/recorder#db_url) avec
```yaml
recorder:
	db_url: mysql://USER:PASSWORD@127.0.0.1:3306/DATABASE?charset=utf8mb4
```

Le mot de passe est celui utilisé pour initialiser la base de donnée.
Utiliser le hachage du mot de passe est plus sécurisé.
```yaml
services:
  db:
    environment:
	  - MARIADB_PASSWORD_HASH=???
```

## MQTT
Le serveur MQTT permet l’échange efficace de messages entre les composants domotiques.

Pour [créer un mot de passe](https://mosquitto.org/man/mosquitto_passwd-1.html), vous pouvez utiliser la commande
```sh
docker run --rm -it -v ./srv/mosquitto/config:/mosquitto/config eclipse-mosquitto:2-openssl mosquitto_passwd -c /mosquitto/config/passwd USER
```

## ESPHome
Ce conteneur génère un serveur pour configurer, compiler et téléverser les programmes esphome vers vos appareils.
Les appareils utilisant esphome peuvent être [liés directement](
https://www.home-assistant.io/integrations/esphome/) à HomeAssistant.

Il est conseillé d’ajouter un mot de passe pour protéger vos configurations et secrets.

# Options
Les services qui doivent accéder à des périphériques comme des adaptateurs sur clé usb peuvent soit ajouter la liaison `devices` soit utiliser un service `privileged: true` (déconseillé).
Pour les périphériques, ceux-ci doivent être d’abord branchés puis ajoutez l’option suivante et reconstruisez le conteneur.
```yaml
services:
  s:
    devices:
	  - /dev/ttyUSB0:/dev/ttyUSB0 # ou
      - /dev/ttyACM0:/dev/ttyACM0 # ex: clé zigbee
```

Les services qui utilisent mDNS doivent utiliser `network_mode: host` car il doivent connaître l’adresse IP de l’hôte pour communiquer.
