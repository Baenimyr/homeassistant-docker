#!/bin/bash
set -eo pipefail

if [ "$MARIADB_RANDOM_ROOT_PASSWORD" ] && [ -z "$MARIADB_USER" ] && [ -z "$MARIADB_PASSWORD" ]; then
	# there's no way we can guess what the random MARIADB password was
	echo >&2 'healthcheck error: cannot determine random root password (and MARIADB_USER and MARIADB_PASSWORD were not set)'
	exit 0
fi

host="localhost"
user="${MARIADB_USER:-root}"
pass="${MARIADB_PASSWORD:-$MARIADB_ROOT_PASSWORD}"

args=(
	# force mysql to not use the local "mysqld.sock" (test "external" connectibility)
	--host="$host"
	--user="$user"
	--password="$pass"
	--silent
)

if command -v mariadb-admin &> /dev/null; then
	if mariadb-admin ${args[@]} ping > /dev/null; then
		exit 0
	fi
else
	if select="$(echo 'SELECT 1' | mariadb "${args[@]}")" && [ "$select" = '1' ]; then
		exit 0
	fi
fi

exit 1
